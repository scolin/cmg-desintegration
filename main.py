#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Thu Sep. 4 18:29:00 2018
All elements required to play to the game Caylus Magna Carta.
@author: Olivier
"""

# Remark: in order to simplify this code, attributes (e.g. Castle.n_castle_tokens,
# SmallProductionPlayerBuilding.n_cubes_into_area, GameElement.n_all_except_last_neutral_buildings) depending on
# the number of players in the game are created as [None .. None, value(n_min_players) .. value(n_max_players)]
# and so are indexed by player numbers in the game.

from src.utils import *
from src.ressources import *
from src.phases import *
from src.players import *
from src.buildings import *
from src.games import *

def main():
    print('Hello.')
    wantToPlay = True
    while wantToPlay == True:
        game = GameElement().game
        game.setup()
        game.play()
        response = input( 'Do you wish to start a new game ? [Y/N]' )
        if(response not in ['Y', 'y']):
            wantToPlay = False
        else:
            print('')
            print( 'Usage: {Beginner|Standard} <color>[=<ai_name>]^2..4' );
            response = input( 'Enter game parameters: ' )
            new_args = response.split(' ')
            for i in range( len(new_args) ):
                sys.argv[i+2] = new_args[i]
    print('Bye.')

if __name__ == "__main__":
    main()
