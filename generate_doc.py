import pydoc
import os

DOC = "doc/"
PKG = "src"

def generate_doc():
    if not os.path.isdir(DOC):
        os.mkdir(DOC)
    for importer, modname, ispkg in pydoc.pkgutil.walk_packages([PKG], ""):
        writedoc(PKG+"."+modname)
    writedoc(PKG)

def writedoc(file: str):
    """Write HTML documentation to a file in the current directory."""
    try:
        object, name = pydoc.resolve(file, 0)
        page = pydoc.html.page(pydoc.describe(object), pydoc.html.document(object, name))
        with open(DOC + "/" + name + '.html', 'w', encoding='utf-8') as file:
            file.write(page)
        print('wrote', name + '.html')
    except (ImportError, pydoc.ErrorDuringImport) as value:
        print(value)

if __name__ == "__main__":
    generate_doc()
