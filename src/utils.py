
from enum import Enum, unique

TXT_SEPARATOR = ', '  # type: str # Separator between two elements of an enumeration of string elements.

def ordinal_number(n: int) -> str:
    """Get the ordinal number."""
    if n <= 0:
        raise Exception('The ordinal number is not defined for non-positive integers.')
    else:
        digit = n % 10  # type: int
        letter_suffix = None  # type: str
        if digit == 1:
            letter_suffix = 'st'
        elif digit == 2:
            letter_suffix = 'nd'
        elif digit == 3:
            letter_suffix = 'rd'
        else:
            letter_suffix = 'th'
        return str(n) + letter_suffix


def indent(n_indent: int) -> str:
    """Get a string in order to create an indentation."""
    return '  ' * n_indent


@unique
class Location(Enum):
    """Enumeration of all the possible locations of the player buildings."""
    HAND = 0
    PILE = 1
    DISCARD = 2
    ROAD = 3
    REPLACED = 4


class Version:
    """All 2 versions of the game: beginner and standard."""

    def __init__(self, name: str):
        """Initialization of a version of the game."""
        # Attributes obtained from the XML file.
        self.name = name  # type: str

    def is_beginner(self) -> bool:
        """Indicates if it is the beginner version; it is the standard version otherwise."""
        return self.name.lower() == 'beginner'

class BuildingMsg:
    ABBREV_NO_USE_EFFECT = 'N'  # type: str[1]
    TXT_NO_USE_EFFECT = '(' + ABBREV_NO_USE_EFFECT + ' if you don\'t want to use the effect)'  # type: str

@unique
class Action(Enum):
    """Enumeration of all the possible actions of the phase Actions."""
    PICK_CARD = ('Pick a card')
    REPLACE_CARDS_IN_HAND = ('Replace all the cards in your hand')
    PLACE_WORKER_ON_BUILDING = ('Place a worker on a building')
    CONSTRUCT_BUILDING_FROM_HAND = ('Construct a building from your hand')
    CONSTRUCT_PRESTIGE_BUILDING_BEGINNER = ('Construct a prestige building (Beginner version)')
    CONSTRUCT_PRESTIGE_BUILDING_STANDARD = ('Construct a prestige building (Standard version)')
    PASSING = ('Passing')

    def __init__(self, txt: str):
        """Initialization of an action."""
        self.txt = txt  # Type: str