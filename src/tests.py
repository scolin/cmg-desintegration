
import unittest

from src.utils import *
from src.ressources import *
from src.players import *

class OrdinalNumberTest(unittest.TestCase):
    """Test cases for ordinal_number function"""

    def test_exception(self):
        """Test if wrong argument throw exeption"""
        for i in [-1, -3, -7]:
            hasFailed = False
            try:
                res = ordinal_number(i)
            except Exception:
                hasFailed = True
            self.assertTrue( hasFailed, 'The function should throw an exception for invalid parameters' )
            
        hasFailed = False
        try:
            res = ordinal_number(0)
        except Exception:
            hasFailed = True
        self.assertTrue( hasFailed, 'The function should throw an exception for invalid parameters' )

    def test_first(self):
        """Test for 'st' suffix"""
        for i in [1, 11, 21]:
            strVal = ordinal_number(i)
            self.assertEqual( strVal, (str(i) + 'st'), 'The function should add "st" suffix' )

    def test_second(self):
        """Test for 'nd' suffix"""
        for i in [2, 12, 22]:
            strVal = ordinal_number(i)
            self.assertEqual( strVal, (str(i) + 'nd'), 'The function should add "nd" suffix' )

    def test_third(self):
        """Test for 'rd' suffix"""
        for i in [3, 13, 23]:
            strVal = ordinal_number(i)
            self.assertEqual( strVal, (str(i) + 'rd'), 'The function should add "rd" suffix' )

    def test_others(self):
        """Test for 'th' suffix"""
        for i in [4, 7, 15, 26, 39, 48]:
            strVal = ordinal_number(i)
            self.assertEqual( strVal, (str(i) + 'th'), 'The function should add "th" suffix' )

class MoneySingletonTest(unittest.TestCase):
    """Test cases for Money Singleton"""

    def test_only_one(self):
        a = Money("Wood", 1)
        b = Money("Iron", 3)
        self.assertIs( a, b )

class RessourceAllPaymentsTest(unittest.TestCase):
    """"Test cases for ressource payments for players"""

    food = Resource('Food', 42)
    wood = Resource('Wood', 42)
    steel = Resource('Steel', 42)
    gold = Resource('Gold', 42)

    @classmethod
    def initPlayer(self):
        Player.n_workers = 0
        Player.n_prestige_pts = 0
        Player.money_resources = []

        color_player = ColorPlayer("red")
        player = BasicAIPlayer(color_player)
        player.setup()
        player.current_money_resources = {
            RessourceAllPaymentsTest.food : 3,
            RessourceAllPaymentsTest.wood : 2,
            RessourceAllPaymentsTest.steel : 3,
            RessourceAllPaymentsTest.gold : 3,
        }
        return player

    def testBasicCost(self):
        player = self.initPlayer()

        ressource_cost = {
            RessourceAllPaymentsTest.food : -1,
            RessourceAllPaymentsTest.wood : -3,
            RessourceAllPaymentsTest.steel : 0,
            RessourceAllPaymentsTest.gold : -1,
        }

        supposed_payments = [{
            RessourceAllPaymentsTest.food : -1,
            RessourceAllPaymentsTest.wood : -2,
            RessourceAllPaymentsTest.steel : 0,
            RessourceAllPaymentsTest.gold : -2,
        }]

        payments = player.resource_all_payments( ressource_cost )

        self.assertEqual( len(payments), len(supposed_payments) )
        for pay in payments:
            self.assertIn( pay, supposed_payments )
    
    def testAdvancedCost(self):
        player = self.initPlayer()

        ressource_cost = {
            RessourceAllPaymentsTest.food : 0,
            RessourceAllPaymentsTest.wood : -1,
            RessourceAllPaymentsTest.steel : 0,
            RessourceAllPaymentsTest.gold : 0,
            None : -1,
        }

        supposed_payments = [{
            RessourceAllPaymentsTest.food : -1,
            RessourceAllPaymentsTest.wood : -1,
            RessourceAllPaymentsTest.steel : 0,
            RessourceAllPaymentsTest.gold : 0,
        },{
            RessourceAllPaymentsTest.food : 0,
            RessourceAllPaymentsTest.wood : -2,
            RessourceAllPaymentsTest.steel : 0,
            RessourceAllPaymentsTest.gold : 0,
        },{
            RessourceAllPaymentsTest.food : 0,
            RessourceAllPaymentsTest.wood : -1,
            RessourceAllPaymentsTest.steel : -1,
            RessourceAllPaymentsTest.gold : 0,
        }]

        payments = player.resource_all_payments( ressource_cost )

        self.assertEqual( len(payments), len(supposed_payments) )
        for pay in payments:
            self.assertIn( pay, supposed_payments )
